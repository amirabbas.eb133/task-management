const Joi = require("@hapi/joi");

exports.ticketValidator = (ticket) => {
  const schema = {
    title: Joi.string().required(),
    description: Joi.string().required(),
    color: Joi.string(),
    label_id: Joi.number(),
  };

  return Joi.validate(ticket, schema);
};
