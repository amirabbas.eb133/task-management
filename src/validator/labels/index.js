const Joi = require("@hapi/joi");

exports.labelValidator = (label) => {
  const schema = {
    name: Joi.string().required(),
    workspace_id: Joi.number(),
  };

  return Joi.validate(label, schema);
};
