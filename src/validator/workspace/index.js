const Joi = require("@hapi/joi");

exports.workspaceValidator = (workspace) => {
  const schema = {
    name: Joi.string().required(),
    background_color: Joi.string(),
  };

  return Joi.validate(workspace, schema);
};
