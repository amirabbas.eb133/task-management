const labels = require("../../models").labels;
const { labelValidator } = require("../../validator/labels");
const { Op } = require("sequelize");

exports.getLabels = async function (req, res) {
  try {
    const { name } = req.body;

    if (!name) {
      const label = await labels.findAll();
      res.send(label);
    } else {
      const label = await labels.findAndCountAll({
        where: { name: { [Op.like]: `%${name}%` } },
        offset: 0,
      });
      res.send(label);
    }
  } catch (error) {
    res.status(500).send(`Something went Wrong ${error}`);
  }
};

exports.createLabel = async function (req, res) {
  try {
    const { name, workspace_id } = req.body;
    const { error } = labelValidator(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const label = await labels.create({ name, workspace_id });
    res.status(201).json(label);
  } catch (error) {
    res.status(500).send(`Something went Wrong ${error}`);
  }
};

exports.updateLabel = async function (req, res) {
  try {
    const { error } = labelValidator(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    const { name } = req.body;
    const id = req.params.id;

    const oldLabel = await labels.findOne({ where: { id: id } });
    if (oldLabel) {
      const label = labels.update(
        { name },
        { returning: true, where: { id: id } }
      );
      res.status(200).send("Successfully Updated!");
    } else {
      res.status(400).send(`label with ${id} is not available yet!`);
    }
  } catch (error) {
    res.status(500).send(`Something went Wrong ${error}`);
  }
};

exports.deleteLabel = async function (req, res) {
  try {
    const id = req.params.id;
    if (!id) res.status(400).send("please pass id ar params!");

    const oldLabel = await labels.findOne({ where: { id: id } });

    if (oldLabel) {
      const label = await labels.destroy({
        where: {
          id: String(id),
        },
      });
      res.status(200).send(`label with ${id} delete successfully!`);
    } else {
      res.status(400).send(`label with ${id} is not available yet!`);
    }
  } catch (error) {
    res.status(500).send(`Something went Wrong ${error}`);
  }
};
