const workspaces = require("../../models").workspaces;
const { workspaceValidator } = require("../../validator/workspace");
const { Op } = require("sequelize");

exports.getWorkspaces = async function (req, res) {
  try {
    const { name } = req.body;

    if (!name) {
      const workspace = await workspaces.findAll();
      res.send(workspace);
    } else {
      const workspace = await workspaces.findAndCountAll({
        where: { name: { [Op.like]: `%${name}%` } },
        offset: 0,
      });
      res.send(workspace);
    }
  } catch (error) {
    res.status(500).send(`Something went Wrong ${error}`);
  }
};

exports.createWorkspace = async function (req, res) {
  try {
    const { name, background_color } = req.body;
    const { error } = workspaceValidator(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const workspace = await workspaces.create({ name, background_color });
    res.status(201).json(workspace);
  } catch (error) {
    res.status(500).send(`Something went Wrong ${error}`);
  }
};

exports.updateWorkspace = async function (req, res) {
  try {
    const { error } = workspaceValidator(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    const { name, background_color } = req.body;
    const id = req.params.id;

    const oldWorkspace = await workspaces.findOne({ where: { id: id } });
    if (oldWorkspace) {
      const workspace = workspaces.update(
        { name, background_color },
        { returning: true, where: { id: id } }
      );
      res.status(200).send("Successfully Updated!");
    } else {
      res.status(400).send(`workspace with ${id} is not available yet!`);
    }
  } catch (error) {
    res.status(500).send(`Something went Wrong ${error}`);
  }
};

exports.deleteWorkspace = async function (req, res) {
  try {
    const id = req.params.id;
    if (!id) res.status(400).send("please pass id ar params!");

    const oldWorkspace = await workspaces.findOne({ where: { id: id } });

    if (oldWorkspace) {
      const workspace = await workspaces.destroy({
        where: {
          id: String(id),
        },
      });
      res.status(200).send(`workspace with ${id} delete successfully!`);
    } else {
      res.status(400).send(`workspace with ${id} is not available yet!`);
    }
  } catch (error) {
    res.status(500).send(`Something went Wrong ${error}`);
  }
};
