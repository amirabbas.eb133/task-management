const tickets = require("../../models").tickets;
const { ticketValidator } = require("../../validator/tickets");
const { Op } = require("sequelize");

exports.getTickets = async function (req, res) {
  try {
    const { title } = req.body;

    if (!title) {
      const ticket = await tickets.findAll();
      res.send(ticket);
    } else {
      const ticket = await tickets.findAndCountAll({
        where: { title: { [Op.like]: `%${title}%` } },
        offset: 0,
      });
      res.send(ticket);
    }
  } catch (error) {
    res.status(500).send(`Something went Wrong ${error}`);
  }
};

exports.createTicket = async function (req, res) {
  try {
    const { title, description, color, label_id } = req.body;
    const { error } = ticketValidator(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const ticket = await tickets.create({
      title,
      description,
      color,
      label_id,
    });
    res.status(201).json(ticket);
  } catch (error) {
    res.status(500).send(`Something went Wrong ${error}`);
  }
};

exports.updateTicket = async function (req, res) {
  try {
    const { error } = ticketValidator(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    const { title, description, color } = req.body;
    const id = req.params.id;

    const oldTicket = await tickets.findOne({ where: { id: id } });
    if (oldTicket) {
      const ticket = tickets.update(
        { title, description, color },
        { returning: true, where: { id: id } }
      );
      res.status(200).send("Successfully Updated!");
    } else {
      res.status(400).send(`ticket with ${id} is not available yet!`);
    }
  } catch (error) {
    res.status(500).send(`Something went Wrong ${error}`);
  }
};

exports.deleteTicket = async function (req, res) {
  try {
    const id = req.params.id;
    if (!id) res.status(400).send("please pass id ar params!");

    const oldTicket = await tickets.findOne({ where: { id: id } });

    if (oldTicket) {
      const ticket = await tickets.destroy({
        where: {
          id: String(id),
        },
      });
      res.status(200).send(`ticket with ${id} delete successfully!`);
    } else {
      res.status(400).send(`ticket with ${id} is not available yet!`);
    }
  } catch (error) {
    res.status(500).send(`Something went Wrong ${error}`);
  }
};
