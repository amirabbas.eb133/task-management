const express = require("express");
const {
  getTickets,
  createTicket,
  updateTicket,
  deleteTicket,
} = require("../../controller/tickets");

const router = express.Router();

router.get("/", getTickets);
router.post("/create", createTicket);
router.put("/update/:id", updateTicket);
router.delete("/delete/:id", deleteTicket);

module.exports = router;
