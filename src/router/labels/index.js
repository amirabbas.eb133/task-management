const express = require("express");
const {
  getLabels,
  createLabel,
  updateLabel,
  deleteLabel,
} = require("../../controller/labels");

const router = express.Router();

router.get("/", getLabels);
router.post("/create", createLabel);
router.put("/update/:id", updateLabel);
router.delete("/delete/:id", deleteLabel);

module.exports = router;
