const express = require("express");
const {
  getWorkspaces,
  createWorkspace,
  updateWorkspace,
  deleteWorkspace,
} = require("../../controller/workspaces");

const router = express.Router();

router.get("/", getWorkspaces);
router.post("/create", createWorkspace);
router.put("/update/:id", updateWorkspace);
router.delete("/delete/:id", deleteWorkspace);

module.exports = router;
