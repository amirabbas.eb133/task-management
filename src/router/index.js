const express = require("express");
const workspaces = require("./workspaces");
const labels = require("./labels");
const tickets = require("./tickets");

const router = express.Router();

router.use("/workspaces", workspaces);
router.use("/labels", labels);
router.use("/tickets", tickets);

module.exports = router;
